(ns koans.core
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))


(defn reverse-interleave [a b] (apply map list (partition b a)))
